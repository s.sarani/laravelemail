<?php

use App\Http\Controllers\SmsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/email/send', [\App\Http\Controllers\NotificationController::class, 'send']);
Route::put('/email/update/{template}', [\App\Http\Controllers\NotificationController::class, 'update']);
Route::get('email/get/{id}', [\App\Http\Controllers\TemplateController::class, 'index']);
Route::post('/sms', [SmsController::class, 'index']);

Route::prefix('/order')
->controller(\App\Http\Controllers\OrderController::class)
->group(function ()
{
 Route::get('/','index');
 Route::post('/store' , 'store');
 Route::put('/update' , 'update');
});

Route::prefix('/invoice')
->controller(\App\Http\Controllers\InvoiceController::class)
->group(function ()
{
 Route::get('/','index');
 Route::post('/store','store');
 Route::put('/update/{invoice}' , 'update');
});

Route::prefix('/product')
->controller(\App\Http\Controllers\ProductController::class)
->group(function ()
{
 Route::get('/','index');
 Route::post('/store','store');
 Route::put('/update/{invoice}' , 'update');
});


Route::prefix('/user')
->controller(\App\Http\Controllers\UserController::class)
->group(function ()
{
 Route::get('/','index');
 Route::get('/getall','getUsres');
 Route::get('/{user_id}/usable','usable');
 Route::post('/store','store');
 Route::put('/update/{invoice}' , 'update');
});

Route::prefix('/service')
->controller(\App\Http\Controllers\ServiceController::class)
->group(function ()
{
 Route::get('/','index');
 Route::get('/expired','getExpired');
 Route::get('/credit','checkCredit');
 Route::get('/found','foundUser');
 Route::post('/store','store');
 Route::put('/update/{invoice}' , 'update');
});

Route::prefix('/cycle')
->controller(\App\Http\Controllers\CycleController::class)
->group(function ()
{
 Route::get('/','index');
 Route::post('/store','store');
 Route::put('/update/{invoice}' , 'update');
});






