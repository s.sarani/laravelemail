<?php

namespace App\Services;

use Ghasedak\GhasedakApi;

class GhasedakService
{

    public function __construct()
    {
    }
    public function ghasedakSendSms($phone_number, $body): void
    {
        try {
            $api = new GhasedakApi(env('GHASEDAKAPI_KEY'));
            $api->SendSimple(
                $phone_number,  // receptor
                $body, // message
                "10008566"    // choose a line number from your account
            );


        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
