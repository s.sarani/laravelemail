<?php

namespace App\Http\Resources;

use App\Models\Invoice;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'product_name'=>$this->product_name,
            'invoice_id'=>new InvocieResource($this->invoice),
            'status'=>$this->status,
            'user'=>$this->user_id
        ];
    }
}
