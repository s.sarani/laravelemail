<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvocieResource extends JsonResource
{
    public static $wrap = 'invoice';
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */

    public function toArray($request)
    {
        return [
            'invoice_id' => $this->id,
            'amountTotal' => $this->amount,
           'expired_at'=>$this->expired_at,
            'status'=>$this->status,
        ];
    }

}
