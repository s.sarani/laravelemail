<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return[
            'user'=>new UserResource($this->whenLoaded('user')),
            'product'=>new ProductResource($this->product),
            'amount'=>$this->product->cycle->amount,
            'status'=>$this->status,
            'active'=>$this->started_at,
            'expired'=>$this->expired_at,
            ];
    }
}
