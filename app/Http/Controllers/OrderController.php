<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvocieResource;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Repositores\Database\OrderRepositroy;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class OrderController extends Controller
{
    /**
     * @var OrderRepositroy
     */
    private $orderRepository;

    public function __construct()
    {
       $this->orderRepository=new OrderRepositroy();
    }

    public function index()
    {
       return OrderResource::collection($this->orderRepository->index());
    }
    public function store(Request $request)
    {

        $data=$request->validate([
            'product_name'=>'required|string',
            'invoice_id'=>'required|integer',
            'status'=>'required|string',
            'user_id'=>'required|integer',
        ]);

        $this->orderRepository->create($data);
    }
    public function update(Order $order)
    {

    }
}
