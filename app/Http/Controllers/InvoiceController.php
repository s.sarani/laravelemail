<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvocieResource;
use App\Models\Invoice;
use App\Repositores\Database\InvoiceRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{

    private $invoiceRepository;
    /**
     * @var Invoice
     */
    private $model;

    public function __construct()
    {
        $this->model=new Invoice();
        $this->invoiceRepository=new InvoiceRepository();
    }

    public function store(Request $request)
    {
        $data=$request->validate([
            'amount'=>'required|integer',
            'expired_at'=>'required|date',
            'status'=>'required|string'
        ]);
        $this->invoiceRepository->create($data);
    }

    public function index()
    {

    return InvocieResource::collection($this->invoiceRepository->index());
    }

    public function update(Invoice $invoice ,Request $request)
    {
        try {
            DB::beginTransaction();
            $this->invoiceRepository->update($invoice,$request->all());
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
        }
    }
}
