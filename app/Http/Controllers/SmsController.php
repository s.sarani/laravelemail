<?php

namespace App\Http\Controllers;

use App\Models\Sms;
use App\Notifications\SmsNotification;

use App\Rules\SmsRule;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public function index(Request $request)
    {
        $data=$request->validate([
           'phone_number'=>'required|numeric',
           'type'=>'required|string',
           'param'=>['required' , 'array' , new SmsRule($request)]
        ]);

        $sms=new Sms();
        $sms->notify(new SmsNotification($data));

    }
}
