<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Repositores\Database\ProductReopsitory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    /**
     * @var ProductReopsitory
     */
    private $productRepository;

    public function __construct()
    {
        $this->productRepository=new ProductReopsitory();
    }

    public function index()
    {
        return ProductResource::collection($this->productRepository->index());
    }
}
