<?php

namespace App\Http\Controllers;

use App\Http\Resources\TemplateResource;
use App\Repositores\Database\TemplateRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TemplateController extends Controller
{
    /**
     * @var TemplateRepository
     */
    protected $model;

    public function __construct()
    {
        $this->model= new TemplateRepository();
    }

    public function index()
    {

      return  TemplateResource::collection($this->model->index());
    }
}
