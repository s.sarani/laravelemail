<?php

namespace App\Http\Controllers;

use App\Jobs\EmailJob;
use App\Models\Template;
use App\Repositores\Database\TemplateRepository;
use App\Rules\TemplateRules;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;

class NotificationController extends Controller
{
    /**
     * @var TemplateRepository
     */
    protected $templaterepository;
    private $template;

    public function __construct()
    {
        $this->templaterepository = new TemplateRepository();
    }

    public function send(Request $request)
    {
        $bacthKey="SendEmail".$request->template;

        $request->validate([
            'email' => 'required|array',
            'template' => ['required', 'string', 'exists:templates,name'],
            'parameters' => ['required', 'array', new TemplateRules($request)],
        ]);
        $this->setTemplate($this->templaterepository->getByName($request->template));
        $data = $this->getTemplate();
        if ($data->is_active == false) {
            return response()->json([
                'errors' => [
                    'status' => 403,
                    'title' => 'Template not active',
                    'detail' => 'Template Is Not Active To use'
                ],
            ], '403');
        }

        $getCache = Cache::get($bacthKey);
        if ($getCache && !Bus::findBatch($getCache)->cancelled()){
            Bus::findBatch($getCache)->add([
                new EmailJob($request->all(), $data)
            ]);
        }else{
            $batch=Bus::batch(
                new EmailJob($request->all(), $data)
            )->name($bacthKey)->dispatch();
            Cache::put($bacthKey, $batch->id);
            }
    }


    public function update(Template $template ,Request $request)
    {
       $data= $request->validate([
           'is_active'=>'numeric'
        ]);
       return $this->templaterepository->update($template , $data);
    }



    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

}
