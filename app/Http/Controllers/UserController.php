<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Jobs\ReminderJob;
use App\Models\Service;
use App\Repositores\Database\CycleRepositroy;
use App\Repositores\Database\ProductReopsitory;
use App\Repositores\Database\ServiceRepository;
use App\Repositores\Database\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Monolog\Handler\IFTTTHandler;
use function Symfony\Component\String\b;

class UserController extends Controller
{
    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->product = new ServiceRepository();
    }

    public function index()
    {
        return UserResource::collection($this->userRepository->index());
    }

    public function usable($user_id)
    {

//        d($user_id);
        //get Users//
        $user = ($this->userRepository->getUser($user_id));
        //credit User//
        $credit = $user->credit;
        //get Service User//
        $serviceActive = $this->getServiceActive($user);
        if ($serviceActive->isEmpty()){
            return response()->json([
               'error'=>[
                   'service'=>'not have service',
                   'user'=>'user not have active service'
               ]
            ],444);
        }
        $user = $this->userRepository->getUser($user_id);
        //credit User
        $credit = $user->credit;
        ////////////////////////
        $sumPriceProduct = 0;
        $activeService = [];
        $productCycle = new CycleRepositroy();
        $cycle = $productCycle->getCycles();
        collect($serviceActive)->each(function ($service) use ($cycle, &$sumPriceProduct) {
            $sumPriceProduct += $cycle[$service->product_id];

        });
        if ($sumPriceProduct <= $credit) {
            list($hours, $credit) = $this->getHours($credit, $sumPriceProduct);
        }
        $serviceArray = $this->setHour($serviceActive, [], $hours);
        $sumPriceProduct = 0;
        $i = 0;
        while (true) {
            $i++;
            collect($serviceActive)->each(function ($service) use (&$cycle, &$sumPriceProduct, $credit, &$serviceActive) {
                if ($sumPriceProduct + $cycle[$service->product_id] <= $credit) {
                    $sumPriceProduct += $cycle[$service->product_id];
                } else {
                    $serviceActive = collect($serviceActive)->where("id", "<>", $service->id);
                }
            });

            if ($sumPriceProduct === 0) {
                break;
            }
            list($hours, $credit) = $this->getHours($credit, $sumPriceProduct);

            $serviceArray = $this->setHour($serviceActive, $serviceArray, $hours);
            $sumPriceProduct = 0;
        }
        collect($serviceArray)->each(function ($hour , $product_id) use($serviceArray , $user ){
            $bacthKey="SendEmail"."HalfDay";
            $product=new ProductReopsitory();
            $products=$product->getProduct($product_id);
            if ($hour == Service::REMINDER_HOUR_HALF_DAY){
                $getCache = Cache::get($bacthKey);
                if ($getCache && !Bus::findBatch($getCache)->cancelled()){
                    Bus::findBatch($getCache)->add([
                        new ReminderJob($user ,$product_id,$products , 'Remindhalf')
                    ]);
                }else{
                    $batch=Bus::batch(
                        new ReminderJob($user ,$product_id,$products,'Remindhalf')
                    )->name($bacthKey)->dispatch();
                    Cache::put($bacthKey, $batch->id);
                }
            }
            if ($hour == Service::REMINDER_HOUR_DAY){
                $bacthKey="SendEmail"."Day";
                $getCache = Cache::get($bacthKey);
                if ($getCache && !Bus::findBatch($getCache)->cancelled()){
                    Bus::findBatch($getCache)->add([
                        new ReminderJob($user ,$product_id , $products ,'Remindday' )
                    ]);
                }else{
                    $batch=Bus::batch(
                        new ReminderJob($user ,$product_id , $products ,'Remindday')
                    )->name($bacthKey)->dispatch();
                    Cache::put($bacthKey, $batch->id);
                }
            }
        });

    }

    public function getUsres()
    {

     $users_id=$this->userRepository->getAllUser();
     $users_id->each(function ($user){
        $this->usable($user->id);
     });
    }


    public function getHours(int $credit, $sumPriceProduct): array
    {
        $hour = (int)($credit / $sumPriceProduct);
        $credit = $credit % $sumPriceProduct;

        return array($hour, $credit);
    }

    private function setHour($activeService, array $array, int $hour)
    {
        collect($activeService)->each(function ($service) use (&$hour, &$array) {
            $array[$service->product_id] = ($array[$service->product_id] ?? 0) + $hour;
        });

        return $array;
    }


    /**
     * @param $user
     * @return mixed
     */
    public function getServiceActive($user)
    {
        $serviceActive = $user->services()->whereStatus(Service::STATUS_ACTIVE)->orderBy('expired_at', 'asc')->get();
        return $serviceActive;
    }
}
