<?php

namespace App\Http\Controllers;

use App\Http\Resources\CycleResource;
use App\Repositores\Database\CycleRepositroy;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CycleController extends Controller
{
    public function __construct()
    {
        $this->cycleRepository=new CycleRepositroy();
    }

    public function index()
    {
        return CycleResource::collection($this->cycleRepository->index());
    }
}
