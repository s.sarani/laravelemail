<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServiceResource;
use App\Jobs\ExpirationServiceJob;
use App\Models\Service;
use App\Repositores\Database\ServiceRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->serviceRepository = new ServiceRepository();
    }

    public function index()
    {
        return ServiceResource::collection($this->serviceRepository->index());
    }

    public function getExpired()
    {
        return ServiceResource::collection($this->serviceRepository->getExpired());
    }

    public function checkCredit()
    {
       ExpirationServiceJob::dispatch();
    }
}
