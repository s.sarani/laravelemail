<?php

namespace App\Repositores\Database;

use App\Models\User;

class UserRepository
{
    public function __construct()
    {
        $this->model=new User();
    }

    public function index()
    {
        return  $this->model->with('service')->get();
    }

    public function lowOffCredit($user_id)
    {
        $this->model->whereId($user_id);
    }

    public function getUser($user_id)
    {
     return $this->model
         ->with('services')
         ->whereId($user_id)
         ->first();

    }

    public function getAllUser()
    {
        return $this->model->with('services')->get();


    }
}
