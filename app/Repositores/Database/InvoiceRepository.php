<?php

namespace App\Repositores\Database;

use App\Models\Invoice;

class InvoiceRepository
{
    /**
     * @var Invoice
     */
    private $model;

    public function __construct()
    {
        $this->model=new Invoice();
    }

    public function create(array $data)
    {
       return $this->model->create($data);
    }

    public function index()
    {
        return $this->model->all();
    }

    public function update(Invoice $invoice,$data)
    {
      return  $invoice->update($data);
    }
}
