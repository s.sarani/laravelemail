<?php

namespace App\Repositores\Database;

use App\Models\Sms;

class SmsRepository
{
    protected $model;

    public function __construct()
    {
        $this->model=new Sms();
    }

    public function get($type)
    {
        return $this->model->whereType($type)->first();
    }

}
