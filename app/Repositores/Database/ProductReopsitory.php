<?php

namespace App\Repositores\Database;

use App\Models\Product;

class ProductReopsitory
{
    public function __construct()
    {
        $this->model=new Product();
    }

    public function index()
    {
       return $this->model->all();
    }

    public function getProduct($product_id)
    {
       return $this->model->where('id',$product_id)->first();
    }
}
