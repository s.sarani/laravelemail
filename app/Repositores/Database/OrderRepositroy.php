<?php

namespace App\Repositores\Database;

use App\Models\Order;

class OrderRepositroy
{
    public function __construct()
    {
        $this->model=new Order();
    }

    public function create(array $data)
    {

       $this->model->create($data);
    }

    public function index()
    {
       return $this->model->all();
    }

    public function update(Order $order, $data)
    {
         tap($order)->update($data);
    }
}
