<?php

namespace App\Repositores\Database;

use App\Models\Service;
use App\Models\User;
use Carbon\Carbon;

class ServiceRepository
{
    public function
    __construct()
    {
        $this->model = new Service();
    }

    public function index()
    {
        return $this->model->with('user')->get();
    }

    public function getExpired()
    {
        return $this->model->with('user')
            ->where('status', Service::STATUS_ACTIVE)
            ->where('expired_at', '<', now())
            ->orderBy('expired_at', 'asc')
            ->get();
    }

    public function updateHourUserActive($user_id)
    {
        $this->model->whereUserId($user_id)->update([
            'expired_at' => Carbon::now()->addHours(1),
        ]);
    }

    public function lowOffCreditUser($user, $amount)
    {
        return $this->model->with('user')->whereId($user)->first();
    }

    public function getProducts($product_id)
    {
        return $this->model
            ->where('product_id', $product_id)
            ->where('status', Service::STATUS_ACTIVE)
            ->orderBy('expired_at', 'asc')
            ->get();
    }
}
