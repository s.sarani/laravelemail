<?php

namespace App\Repositores\Database;

use App\Models\Cycle;

class CycleRepositroy
{
    public function __construct()
    {
        $this->model = new Cycle();
    }

    public function index()
    {
        return  $this->model->all();
    }

    public function getProduct($product_id)
    {
        return $this->model->where('product_id' , $product_id)->first();
    }

    public function getCycles()
    {
       return $this->model->pluck('amount' , 'product_id');
    }
}
