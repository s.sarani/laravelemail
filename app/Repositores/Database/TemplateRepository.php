<?php

namespace App\Repositores\Database;

use App\Models\Template;

class TemplateRepository
{
    /**
     * @var Template
     */
    private $model;

    public function __construct()
    {
    $this->model=new Template();
    }

    public function getByName($template)
    {
        return $this->model->whereName($template)->first();
    }

    public function index()
    {
      return  $this->model->all();
    }

    public function update(Template $template , $data)
    {

        return tap($template)->update($data);
    }
}
