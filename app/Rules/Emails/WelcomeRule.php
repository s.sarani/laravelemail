<?php

namespace App\Rules\Emails;

class WelcomeRule implements IEmailRule
{

    private $request;

    public function __construct($request)
    {

        $this->request=$request;
    }
    public function validate()
    {
        $this->request->validate([
            'parameters.name'=>'required|string'
        ]);
        return true;
    }
}
