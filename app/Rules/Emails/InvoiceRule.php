<?php

namespace App\Rules\Emails;
use Illuminate\Http\Request;

class InvoiceRule implements IEmailRule
{


    protected $request;

    public function __construct($request=null)
    {
        $this->request=$request;
    }

    public function validate()
    {
       $this->request->validate([
         'parameters.invoice_id'=>'required|integer',
         'parameters.name'=>'required|string',
         'subject.invoice_id'=>'required|integer',
     ]);
    return true;
    }
}
