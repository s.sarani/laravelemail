<?php

namespace App\Rules\Sms;

class WelcomeSmsRule implements ISmsRules
{

    protected $request;

    public function __construct($request)
    {
      $this->request=$request;
    }

    public function validate()
    {
        $this->request->validate([
            'param.name'=>'required|string',
        ]);
        return true;

    }
}
