<?php

namespace App\Rules\Sms;

class LoginfailedSmsRule implements ISmsRules
{

    protected $request;

    public function __construct($request)
    {
        $this->request=$request;
    }

    public function validate()
    {
        $this->request->validate([
            'param.tries'=>'required|integer',
            'param.name'=>'required|string',

        ]);
        return true;

    }
}
