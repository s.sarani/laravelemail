<?php

namespace App\Rules\Sms;

interface ISmsRules
{
    public function __construct($request);
    public function validate();

}
