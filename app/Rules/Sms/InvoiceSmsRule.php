<?php

namespace App\Rules\Sms;

class InvoiceSmsRule implements ISmsRules
{

    protected $request;

    public function __construct($request)
    {
        $this->request=$request;
    }

    public function validate()
    {
        $this->request->validate([
            'param.invoice_id'=>'required|integer',
            'param.name'=>'required|string',
        ]);
        return true;

    }
}
