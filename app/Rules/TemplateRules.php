<?php

namespace App\Rules;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class TemplateRules implements Rule
{
    /**
     * @var mixed|null
     */
    protected $request;
    protected $pathview;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request = null)
    {
        $this->request = $request;
        $this->template = $request->template;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $template = $this->template;

        $nameTemplate = Str::studly($template);
        $nameTemplate .= "Mail.blade.php";
        $nameTemplate = resource_path("views/mail/$nameTemplate");
        if (file_exists($nameTemplate)) {
            $nameClass=Str::studly($this->request['template']);
            $className = "\App\Rules\Emails\\{$nameClass}";
            $className .= "Rule";
            if (class_exists($className)) {
                $class = new $className($this->request);
                $class->validate();
            } else {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Template Not Exist!';
    }
}
