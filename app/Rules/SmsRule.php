<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class SmsRule implements Rule
{
    protected $type;
    protected $request;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
       $this->type=$request->type;
       $this->request=$request;

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $type = $this->type;
        $type = Str::studly($type);
        $type .= "SmsRule";
        if (file_exists(__DIR__ . "/Sms/$type.php")) {

            $className = "App\Rules\Sms\\{$type}";
            if (class_exists($className)) {
                $class = new $className($this->request);
                $class->validate();

            } else {

                return false;
            }
        } else {

            return false;
        }

        return true;


    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error message.';
    }
}
