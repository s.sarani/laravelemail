<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RemindeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $product;
    private $template;
    public $service;
    private $product_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template ,$service , $product)
    {
        $this->subject='REMINDER';
        $this->service=$service;
        $this->product=$product;
        $this->template=$template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.'.$this->template."Mail");
    }
}
