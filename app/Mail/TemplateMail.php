<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;

class TemplateMail extends Mailable
{
    use Queueable, SerializesModels;


    public $parameters;
    public $template;
    public $subject;
    protected $viewMap;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($template, $parameters, $request,$objectTemplate)
    {

        $this->template = $template;
        $this->parameters = $parameters;
        $this->subject =$this->createSubject($request ,$objectTemplate);
        $this->setViewMap($objectTemplate->view_map);

    }

    /**
     * @return mixed
     */
    public function getViewMap()
    {
        return $this->viewMap;
    }

    /**
     * @param mixed $viewMap
     */
    public function setViewMap($viewMap): void
    {
        $this->viewMap = $viewMap;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

       return $this->view($this->getViewMap());
    }

    public function createSubject($request, $dbsubject): string
    {
        $str = $dbsubject->subject;
        $subject=$request['subject']??'';
        collect($subject)->each(function ($value, $key) use (&$str) {
            $str = Str::replace('{$' . $key . '}', "$value", $str);
        });
        return $str;
    }
}
