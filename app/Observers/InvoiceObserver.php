<?php

namespace App\Observers;

use App\Models\invoice;
use App\Models\Order;
use App\Repositores\Database\OrderRepositroy;

class InvoiceObserver
{
    public function __construct()
    {

    }

    /**
     * Handle the invoice "created" event.
     *
     * @param \App\Models\invoice $invoice
     * @return void
     */
    public function created(invoice $invoice)
    {
        //
    }

    /**
     * Handle the invoice "updated" event.
     *
     * @param \App\Models\invoice $invoice
     * @return void
     */
    public function updated(invoice $invoice)
    {
        if ($invoice->isDirty('status') && strtolower($invoice->status) == Invoice::STATUS_PAID)
        {
            $orderRepository=new OrderRepositroy();
            $orderRepository->update($invoice->order, ['status'=>Order::STATUS_PAID]);
        }
    }

    /**
     * Handle the invoice "deleted" event.
     *
     * @param \App\Models\invoice $invoice
     * @return void
     */
    public function deleted(invoice $invoice)
    {
        //
    }

    /**
     * Handle the invoice "restored" event.
     *
     * @param \App\Models\invoice $invoice
     * @return void
     */
    public function restored(invoice $invoice)
    {
        //
    }

    /**
     * Handle the invoice "force deleted" event.
     *
     * @param \App\Models\invoice $invoice
     * @return void
     */
    public function forceDeleted(invoice $invoice)
    {
        //
    }
}
