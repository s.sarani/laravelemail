<?php

namespace App\Observers;

use App\Models\Template;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;

class TemplateObserver
{
    /**
     * Handle the Template "created" event.
     *
     * @param  \App\Models\Template  $template
     * @return void
     */
    public function created(Template $template)
    {
        //
    }

    /**
     * Handle the Template "updated" event.
     *
     * @param  \App\Models\Template  $template
     * @return void
     */
    public function updated(Template $template)
    {
        $bacthKey="SendEmail".$template->name;
        $getCache=Cache::get($bacthKey);
        if ($template->isDirty('is_active') && $template->is_active==false){
            Bus::findBatch($getCache)->cancel();
        }
    }

    /**
     * Handle the Template "deleted" event.
     *
     * @param  \App\Models\Template  $template
     * @return void
     */
    public function deleted(Template $template)
    {
        //
    }

    /**
     * Handle the Template "restored" event.
     *
     * @param  \App\Models\Template  $template
     * @return void
     */
    public function restored(Template $template)
    {
        //
    }

    /**
     * Handle the Template "force deleted" event.
     *
     * @param  \App\Models\Template  $template
     * @return void
     */
    public function forceDeleted(Template $template)
    {
        //
    }
}
