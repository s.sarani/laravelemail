<?php

namespace App\Jobs;

use App\Mail\RemindeMail;
use App\Mail\TemplateMail;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ReminderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels , Batchable;

    private $template;
    private $product;
    private $email;
    private $product_id;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user , $product_id ,$products ,$template)
    {

        $this->email=$user->email;
        $this->product_id=$product_id;
        $this->template=$template;
        $this->product=$products;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->email)->send(new RemindeMail($this->template, $this->product_id , $this->product));
    }
}
