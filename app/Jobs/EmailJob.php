<?php

namespace App\Jobs;

use App\Mail\TemplateMail;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use SebastianBergmann\Diff\Exception;


class EmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels ,Batchable;

    public $request;
    public $templatename;
    protected $parameters;
    public $subject;
    protected $template;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request, $getTemplate)
    {

        $this->request = $request;
        $this->template=$getTemplate;
        $this->templatename = Str::ucfirst($getTemplate->name);
        $this->parameters = $request['parameters'];
        $this->templatename .= 'Mail';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            $bacthKey="SendEmail".$this->template->name;
            if (Bus::findBatch(Cache::get($bacthKey))->canceled() && $this->template->is_active==0){
                $this->fail();
                throw new \Exception('Template Deactive Because Dont Send');
            }else {
                Mail::to($this->request['email'])->send(new TemplateMail($this->templatename, $this->parameters, $this->request, $this->template));
            }
    }



}
