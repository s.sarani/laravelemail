<?php

namespace App\Jobs;

use App\Models\Service;
use App\Repositores\Database\ServiceRepository;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExpirationServiceJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var ServiceRepository
     */
    private $serviceRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->serviceRepository = new ServiceRepository();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $services = $this->serviceRepository->getExpired();
        collect($services)->each(function ($item) {
            $amount = $item->product->cycle->amount;
            $credit = $item->user->credit;
            if ($amount <= $credit) {
                try {
                    DB::beginTransaction();
                    $item->user->credit -= $amount;
                    $item->expired_at = Carbon::now()->addHour(1);
                    $item->user->save();
                    $item->save();
                    DB::commit();
                    Log::info('This Service Was Updated:', [$item]);
                } catch (\Exception $e) {
                    Log::error('This Service Was Canceled Because : ', [$e->getMessage()]);
                    DB::rollBack();
                }
            } else {
                $item->status = Service::STATUS_EXPIRED;
                $item->save();
            }
        });
    }
}
