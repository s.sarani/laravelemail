<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
    const STATUS_PAID='paid';
    const STATUS_PENDING='pending';
    const STATUS_CANCELED='canceled';


    protected $fillable = [
        'amount',
        'expired_at',
        'status',

    ];
    protected $guarded=[];

    public function order()
    {
        return $this->hasOne(Order::class);
    }
}
