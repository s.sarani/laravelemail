<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'view_map',
        'is_active',
        'subject',
    ];
    protected $casts = [
        'is_active'=>'boolean'
    ];
    protected  $hidden=[
        'created_at',
        'updated_at'
    ];
}
