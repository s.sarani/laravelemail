<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    const STATUS_PENDING='pending';
    const STATUS_CANCELED='canceled';
    const STATUS_ACTIVE='active';
    const STATUS_EXPIRED='expired';
    const REMINDER_HOUR_HALF_DAY=12;
    const REMINDER_HOUR_DAY=24;


    use HasFactory;
    protected $guarded=[];

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
