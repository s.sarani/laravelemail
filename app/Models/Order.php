<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    const STATUS_PAID='paid';
    const STATUS_PENDING='pending';
    const STATUS_CANCELED='canceled';
    protected $fillable=[
      'product_name',
      'invoice_id',
       'status',
        'user_id'
    ];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
