<?php

namespace App\Broadcasting;

use App\Models\Delivery;
use App\Models\Sms;
use App\Models\User;
use App\Notifications\SmsNotification;
use App\Repositores\Database\SmsRepository;
use App\Services\GhasedakService;
use Ghasedak\GhasedakApi;
use Illuminate\Support\Str;

class SmsChannel
{
    /**
     * @var SmsRepository
     */
    protected $smsRepository;
    /**
     * @var GhasedakService
     */
    private $ghasedakService;

    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->smsRepository=new SmsRepository();
        $this->ghasedakService=new GhasedakService();
    }
    /**
     * Authenticate the user's access to the channel.
     *
     * @param \App\Models\Sms $sms
     * @return array|bool
     */
    public function send($notifiable, SmsNotification $notification)
    {
        $data =$this->getArgumentNotification($notification, $notifiable);
        $sms =$this->smsRepository->get($data['type']) ;
        $body=$this->createBody($notifiable, $sms['body'],$notification);
        $this->ghasedakService->ghasedakSendSms($data['phone_number'], $body);
    }
    public function createBody($notifiable, $body, SmsNotification $notification): string
    {
        $str = $body;
        $data = $this->getArgumentNotification($notification, $notifiable);
        collect($data['param'])->each(function ($value, $key) use (&$str) {
            $str = Str::replace('{$' . $key . '}', "$value", $str);
        });

        return $str;
    }
    public function getArgumentNotification(SmsNotification $notification, $notifiable)
    {
        $data = $notification->toSms($notifiable);

        return $data;
    }




}
