<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CycleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id'=>rand(1 , 10),
            'amount'=>$this->faker->randomElement(['100','500','1000']),
            'period'=>('hourly'),
        ];
    }
}
