<?php

namespace Database\Factories;

use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServiceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'=>rand(1,10),
            'product_id'=>rand(1,10),
            'status'=>$this->faker->randomElement([
//                Service::STATUS_CANCELED,
//                Service::STATUS_PENDING,
                Service::STATUS_ACTIVE,
                Service::STATUS_EXPIRED,
            ]),

            'started_at'=>Carbon::now()
                ->subDay()
                ->addHours(rand(1 ,4))
                ->addMinutes(rand(1 , 59))
                ->addSeconds(rand(1 , 59)),

            'expired_at'=>Carbon::now()
                ->addMinutes(2)
                ->addSeconds(rand(1 , 59)),
        ];
    }
}
