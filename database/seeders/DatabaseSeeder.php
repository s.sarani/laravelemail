<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Event;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
//            ProductSeeder::class,
//            UserSeeder::class,
//            ProductSeeder::class,
            ServiceSeeder::class,
//            CycleSeeder::class,
        ]);
    }
}
